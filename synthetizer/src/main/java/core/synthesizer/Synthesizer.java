package core.synthesizer;

import com.jsyn.JSyn;
import com.jsyn.unitgen.UnitGenerator;
import core.units.Unit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chouaib on 03/02/17.
 */
public class Synthesizer implements ISynthesizer {

    private static Synthesizer synth = null;
    private static com.jsyn.Synthesizer synthesizer = null;
    private List<Unit> units = new ArrayList();


    public static Synthesizer getSynthesizer() {
        if (synth == null) {
            synth = new Synthesizer();
            synthesizer = JSyn.createSynthesizer();
        }
        return synth;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public boolean isRunning() {
        return this.synthesizer.isRunning();
    }

    public void start() {
        if (!this.isRunning()) {
            this.synthesizer.start();
            for (int i = 0; i < units.size(); i++) {
                units.get(i).start();
            }
        }
    }

    public void stop() {
        if (this.isRunning()) {
            this.synthesizer.stop();
            for (int i = 0; i < units.size(); i++) {
                units.get(i).stop();
            }
        }
    }

    public void startUnit(Unit unit) {
        unit.start();
    }

    public void stopUnit(Unit unit) {
        unit.stop();
    }

    public void add(Unit unit) {
        if (this.synthesizer != null && unit != null) {
            this.units.add(unit);
            unit.addToSynthesizer(this);
        }
    }

    public void remove(Unit unit) {
        if (this.synthesizer != null && unit != null) {
            this.units.remove(unit);
            unit.removeFromSynthesizer(this);
        }
    }

    public void add(UnitGenerator unitGen) {
        if (this.synthesizer != null && unitGen != null) {
            this.synthesizer.add(unitGen);
        }
    }

    public void remove(UnitGenerator unitGen) {
        if (this.synthesizer != null && unitGen != null) {
            this.synthesizer.remove(unitGen);
        }
    }
}