package client.guibank.components.element;

import client.guibank.components.Component;
import client.guibank.components.Geometry;
import com.jsyn.ports.UnitPort;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sarah on 07/02/17.
 */
public class Port extends Geometry {

    private static List<Port> ports=new ArrayList<Port>();
    private Image image;
    private boolean isInput = false;
    private UnitPort unitPort;
    private Component component;

    private Port(String imgResource, double x, double y, UnitPort unitPort, boolean isInput, Component component) {
        this.x = x;
        this.y = y;
        this.isInput = isInput;
        this.unitPort = unitPort;
        this.component = component;
        this.image = new Image(this.getClass().getResource(imgResource).toString());
        this.width=30;
        this.height=30;
    }

    public Component getComponent() {
        return component;
    }

    public void draw(GraphicsContext gc) {
        gc.drawImage(this.image, this.x, this.y,this.width,this.height);
    }

    private boolean equals(Port port){
        return (port.getUnitPort()==this.unitPort && port.getComponent()==this.component);
    }

    public static Port getPort(String imgResource, double x, double y, UnitPort unitPort, boolean isInput, Component component){
        Port newPort = new Port(imgResource,x,y,unitPort,isInput,component);
        for(Port p:ports){
            if(newPort.equals(p)){
                p.move(x,y);
                return p;
            }
        }
        ports.add(newPort);
        return newPort;
    }


    public boolean isInput() {
        return isInput;
    }

    public UnitPort getUnitPort() {
        return unitPort;
    }

    public void handleClick(double x, double y) {}

    public void destroy() {

    }

    public boolean needToBeDestroy() {
        return false;
    }
}
