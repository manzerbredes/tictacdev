package core.units.osc;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public final class OscilloscopeRectangleCanvas {
	
	private static int Y_SCALE = 10;
	
	private static int Y_WIDTH = 200;
	
	private static int X_WIDTH = 350;

	public static void initiateOscilloscope(GraphicsContext graph, int x, int y) {
		graph.setFill(Color.MAROON);
		graph.fillRect(x, y, X_WIDTH, Y_WIDTH);
		dessinerLines510(graph, x, y);
	}

	private static void dessinerLines510(GraphicsContext graph, int x, int y) {
		graph.setFill(Color.BLACK);
		graph.setLineWidth(2);
		graph.strokeLine(x, y + Y_WIDTH/2, x + X_WIDTH, y + Y_WIDTH/2);
		graph.strokeLine(x + 2, y + 2, x + 2, y + Y_WIDTH);
		graph.strokeLine(x, y + Y_WIDTH/4, x + X_WIDTH, y + Y_WIDTH/4);
		graph.strokeLine(x, y + Y_WIDTH * 3/4, x + X_WIDTH, y + Y_WIDTH * 3/4);
		graph.strokeLine(x, y + 1, x + X_WIDTH, y + 1);
		graph.strokeLine(x + X_WIDTH/5, y, x + X_WIDTH/5, y + Y_WIDTH);
		graph.strokeLine(x + X_WIDTH*2/5, y, x + X_WIDTH*2/5, y + Y_WIDTH);
		graph.strokeLine(x + X_WIDTH*3/5, y, x + X_WIDTH*3/5, y + Y_WIDTH);
		graph.strokeLine(x + X_WIDTH*4/5, y, x + X_WIDTH*4/5, y + Y_WIDTH);
		graph.strokeLine(x + X_WIDTH, y, x + X_WIDTH, y + Y_WIDTH);
		graph.strokeLine(x, y + Y_WIDTH, x + X_WIDTH, y + Y_WIDTH);
		graph.setFill(Color.AQUA);
		graph.fillText(" 0", x, y + Y_WIDTH/2 + 7);
		graph.fillText(" 5 V", x, y + Y_WIDTH/4 + 2);
		graph.fillText(" -5 V", x, y + Y_WIDTH * 3/4);
		graph.fillText(" 10 V", x + 2, y + 5);
		graph.fillText(" -10 V", x, y + Y_WIDTH);
	}

	public static void dessinerSignal(SignalCapture signalCapture, GraphicsContext graph, int x, int y) {
		signalCapture.initCapture();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		graph.beginPath();
		graph.setStroke(Color.ORANGE);
		int x_absis = x;
		graph.moveTo(x_absis, y + Y_WIDTH/2);
		if (signalCapture.getListBips().size()>0) {
			graph.moveTo(x_absis + 1, y + Y_WIDTH/2 - signalCapture.getListBips().get(0)* Y_SCALE);
		}
		//System.out.println("signalCapture.getListBips() = " + signalCapture.getListBips());
		for(Double bip : signalCapture.getListBips()) {
			x_absis = x_absis + 1;
			graph.lineTo(x_absis, y + Y_WIDTH/2 - bip* Y_SCALE);
		}
		graph.stroke();
		graph.setStroke(Color.BLACK);
	}
	
}
