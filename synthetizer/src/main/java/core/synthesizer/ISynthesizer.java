package core.synthesizer;

import com.jsyn.unitgen.UnitGenerator;
import core.units.Unit;


/**
 * Created by chouaib on 03/02/17.
 */
public interface ISynthesizer {

    /**
     * Start the synthesizer
     */
    void start();

    /**
     * Stop the synthesizer
     */
    void stop();

    /**
     * Check if the synthesizer is running
     * @return
     */
    boolean isRunning();

    /**
     * Start a specific unit
     * @param unit
     */
    void startUnit(Unit unit);

    /**
     * Stop a specific unit
     * @param unit
     */
    void stopUnit(Unit unit);

    /**
     * Add a unit
     * @param unit
     */
    void add(Unit unit);

    /**
     * Remove a unit
     * @param unit
     */
    void remove(Unit unit);


    /**
     * Add a UnitGenerator
     * @param unitGen
     */
    void add(UnitGenerator unitGen);

    /**
     * Remove a UnitGenerator
     * @param unitGen
     */
    void remove(UnitGenerator unitGen);


}
