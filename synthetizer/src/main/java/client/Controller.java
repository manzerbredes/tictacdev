package client;

import client.guibank.GUIBank;
import client.guibank.GUIBankImpl;
import client.guibank.components.EG;
import client.guibank.components.OSC;
import client.guibank.components.OUT;
import client.guibank.components.REP;
import client.guibank.components.VCA;
import client.guibank.components.VCO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;


public class Controller {


    @FXML
    private Canvas canvas;

    @FXML
    private Group cableGroup;

    @FXML
    private ColorPicker colorPicker;

    private GUIBank guiBank;


    @FXML
    public void initialize() {
        this.colorPicker.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                guiBank.setCableColor(colorPicker.getValue());
            }
        });
        this.colorPicker.setValue(Color.BROWN);
        this.guiBank = new GUIBankImpl(canvas, this.cableGroup);
        this.guiBank.setCableColor(this.colorPicker.getValue());

        this.canvas.getGraphicsContext2D().setFill(Color.WHITE);

        guiBank.addComponent(new OUT());

    }

    @FXML
    public void buildVCO() {
        this.guiBank.addComponent(new VCO());
    }

    @FXML
    public void buildVCA() {
        this.guiBank.addComponent(new VCA());
    }

    @FXML
    public void buildREP() {
        this.guiBank.addComponent(new REP());
    }
    
    @FXML
    public void buildEG() {
        this.guiBank.addComponent(new EG());
    }
    
    @FXML
    public void buildOSC() {
        this.guiBank.addComponent(new OSC());
    }
}