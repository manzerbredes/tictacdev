package core.portObserver;

import client.guibank.components.element.Port;

/**
 * Created by loic on 10/02/17.
 */
public interface PortObservable {

    void connect(Port port);

    void disconnect(Port port);

    void addObserver(PortObserver observer);

    void removeObserver(PortObserver observer);

}
