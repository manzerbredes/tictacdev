package client.guibank.components;


import client.guibank.components.element.Button;
import client.guibank.components.element.CircleSlider;
import client.guibank.components.element.Port;
import core.portObserver.PortObservable;
import core.portObserver.PortObserver;
import core.synthesizer.Synthesizer;
import core.units.vcoa.VCOA;
import core.units.vcoa.VCOAImpl;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sarah on 07/02/17.
 */
public class VCO extends Geometry implements Component, PortObservable {

    private Button backBaseFrequency, nextBaseFrequency, backFrequency, nextFrequency, delete;
    private CircleSlider baseFrequency;
    private int baseFrequencyAngle = 0, frequencyAngle = 0;
    private CircleSlider frequency;
    private List<PortObserver> observers = new ArrayList<PortObserver>();
    private Port portFm, portOut;
    private VCOA vcoa = new VCOAImpl();
    private boolean needToBeDestroy = false;

    public VCO() {
        Synthesizer.getSynthesizer().add(vcoa);
        Synthesizer.getSynthesizer().startUnit(vcoa);

        this.width = 250;
        this.height = 150;

    }

    private void buildElements() {
        this.delete = new Button("/imgs/delete.png", this.x + this.width - 25, this.y + 5);

        this.baseFrequency = new CircleSlider("/imgs/button.png", 0, 0, this.baseFrequencyAngle);
        this.baseFrequency.move((this.x + this.width / 2 - this.baseFrequency.getWidth() / 2) - 35, (this.y + this.height / 2) - this.baseFrequency.getHeight() / 2);

        this.backBaseFrequency = new Button("/imgs/previous.png", this.x + (this.width / 2) - 65, ((this.y + this.height / 2) + 25));
        this.nextBaseFrequency = new Button("/imgs/next.png", this.x + (this.width / 2) - 35, ((this.y + this.height / 2) + 25));

        this.frequency = new CircleSlider("/imgs/button.png", 0, 0, this.frequencyAngle);
        this.frequency.move((this.x + this.width / 2 - this.frequency.getWidth() / 2) + 35, (this.y + this.height / 2) - this.frequency.getHeight() / 2);

        this.backFrequency = new Button("/imgs/previous.png", this.x + (this.width / 2) + 5, ((this.y + this.height / 2) + 25));
        this.nextFrequency = new Button("/imgs/next.png", this.x + (this.width / 2) + 35, ((this.y + this.height / 2) + 25));

        this.portFm = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) - 20, vcoa.getInputPort(0), true, this);

        this.portOut = Port.getPort("/imgs/out.png", (this.x + this.width) - 38, this.y + (this.height / 2) - 20, vcoa.getOutputPort(0), false, this);
    }


    public void draw(GraphicsContext gc) {

        this.buildElements();
        gc.setFill(Color.valueOf("#626567"));
        gc.fillRect(this.x, this.y, this.width, this.height);
        gc.setStroke(Color.BLACK);
        gc.strokeRoundRect(this.x, this.y, this.width, this.height, 5, 5);
        gc.setFill(Color.WHITE);
        gc.setStroke(Color.WHITE);
        gc.fillText("VCO", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));
        gc.strokeText("VCO", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));

        gc.fillText("FM", this.x + 16, this.y + (this.height / 2 - 22));
        gc.fillText("OUT", (this.x + this.width) - 38, this.y + (this.height / 2) - 22);

        backBaseFrequency.draw(gc);
        nextBaseFrequency.draw(gc);
        baseFrequency.draw(gc);

        backFrequency.draw(gc);
        nextFrequency.draw(gc);
        frequency.draw(gc);

        portFm.draw(gc);
        portOut.draw(gc);

        delete.draw(gc);

    }

    public void handleClick(double x, double y) {
        if (backBaseFrequency.isClickedRect(x, y)) {
            this.baseFrequencyAngle -= 40;
            this.vcoa.setBaseFrequency(this.vcoa.getBaseFrequency() - 1);
        } else if (nextBaseFrequency.isClickedRect(x, y)) {
            this.baseFrequencyAngle += 40;
            this.vcoa.setBaseFrequency(this.vcoa.getBaseFrequency() + 1);
        } else if (backFrequency.isClickedRect(x, y)) {
            this.frequencyAngle--;
            this.vcoa.setFrequency(this.vcoa.getFrequency() - 5);
        } else if (nextFrequency.isClickedRect(x, y)) {
            this.frequencyAngle++;
            this.vcoa.setFrequency(this.vcoa.getFrequency() + 5);

        } else if (portOut.isClicked(x, y)) {
            connect(portOut);
        } else if (this.portFm.isClicked(x, y)) {
            connect(this.portFm);
        } else if (this.delete.isClicked(x, y)) {
            this.needToBeDestroy = true;
        }


    }

    public void destroy() {
        disconnect(portFm);
        disconnect(portOut);
        Synthesizer.getSynthesizer().stopUnit(this.vcoa);
        Synthesizer.getSynthesizer().remove(this.vcoa);
    }

    public boolean needToBeDestroy() {
        return this.needToBeDestroy;
    }

    public void connect(Port port) {
        for (PortObserver o : this.observers) {
            o.connect(port);
        }
    }

    public void disconnect(Port port) {
        for (PortObserver o : this.observers) {
            o.disconnect(port);
        }
    }

    public void addObserver(PortObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(PortObserver observer) {
        this.observers.remove(observer);

    }

}
