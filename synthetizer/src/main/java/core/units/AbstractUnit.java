package core.units;

import client.guibank.components.Geometry;
import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by loic on 05/02/17.
 */
public abstract class AbstractUnit implements Unit {


    /**
     * Ports IDs
     */
    protected Geometry component;
    protected List<Integer> inputPortsIds = new ArrayList<Integer>();
    protected List<Integer> outputPortsIds = new ArrayList<Integer>();


    /**
     * @return
     * @see Unit
     */
    public List<Integer> getInputPorts() {
        return this.inputPortsIds;
    }

    /**
     * @return
     * @see Unit
     */
    public List<Integer> getOutputPorts() {
        return this.outputPortsIds;
    }

    /**
     * Build ports list ids
     *
     * @param nbInput
     * @param nbOutput
     */
    protected void setPortsNumbers(int nbInput, int nbOutput) {
        this.inputPortsIds.clear();
        this.outputPortsIds.clear();
        for (int i = 0; i < nbInput; i++) {
            this.inputPortsIds.add(i);
        }
        for (int i = 0; i < nbOutput; i++) {
            this.outputPortsIds.add(i);
        }
    }

    /**
     * Disconnect input
     *
     * @param inputId
     */
    public void disconnectAllInput(int inputId) {
        if (isValidPort(true, inputId)) {
            this.getInputPort(inputId).disconnectAll();
        }
    }

    /**
     * Disconnect output
     *
     * @param ouputId
     */
    public void disconnectAllOutput(int ouputId) {
        if (isValidPort(false, ouputId)) {
            this.getOutputPort(ouputId).disconnectAll();
        }
    }

    /**
     * Check if a port is valid
     *
     * @param isInput
     * @param id
     * @return
     */
    protected boolean isValidPort(boolean isInput, int id) {
        if (isInput) {
            if (id < this.inputPortsIds.size()) {
                return true;
            }
        } else {
            if (id < this.outputPortsIds.size()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param inputId
     * @param unit
     * @param outputId
     * @see Unit
     */
    public void disconnectInput(int inputId, Unit unit, int outputId) {
        if (isValidPort(true, inputId)) {
            this.getInputPort(inputId).disconnect(unit.getOutputPort(outputId));
        }
    }

    /**
     * @param outputId
     * @param unit
     * @param inputId
     * @see Unit
     */
    public void disconnectOutput(int outputId, Unit unit, int inputId) {
        if (isValidPort(false, outputId)) {
            this.getOutputPort(outputId).disconnect(unit.getInputPort(inputId));
        }
    }

    /**
     * @param inputId
     * @param unit
     * @param outputId
     * @see Unit
     */
    public void connectInput(int inputId, Unit unit, int outputId) {
        if (isValidPort(true, inputId)) {
            UnitOutputPort out = unit.getOutputPort(outputId);
            if (out != null) {
                this.getInputPort(inputId).connect(out);
            }
        }
    }

    /**
     * @param outputId
     * @param unit
     * @param inputId
     * @see Unit
     */
    public void connectOutput(int outputId, Unit unit, int inputId) {
        if (isValidPort(false, outputId)) {
            UnitInputPort in = unit.getInputPort(inputId);
            if (in != null) {
                this.getOutputPort(outputId).connect(in);

            }
        }
    }

}
