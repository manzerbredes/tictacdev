package core.units.eg;

import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;
import com.jsyn.unitgen.EnvelopeDAHDSR;

import core.synthesizer.Synthesizer;
import core.units.AbstractUnit;

public class EGUnitImpl extends AbstractUnit {

	private EGEnveloppeImpl egUnitGenerator;

	public EGEnveloppeImpl getEgUnitGenerator() {
		return egUnitGenerator;
	}

	public void setEgUnitGenerator(EGEnveloppeImpl egUnitGenerator) {
		this.egUnitGenerator = egUnitGenerator;
	}

	public EGUnitImpl() {
		super();
		this.egUnitGenerator = new EGEnveloppeImpl();
		this.setPortsNumbers(1, 1);
	}

	@Override
	public void start() {
		this.egUnitGenerator.start();
	}

	@Override
	public void stop() {
		this.egUnitGenerator.stop();
	}

	@Override
	public UnitInputPort getInputPort(int id) {
		if(id==0){
            return this.egUnitGenerator.input;
        }
        return null;
	}


	public UnitOutputPort getOutputPort(int id) {
		if(id==0){
			return this.egUnitGenerator.output;
		}return null;
	}

	public void addToSynthesizer(Synthesizer synthesizer) {
		synthesizer.add(this.egUnitGenerator);
	}

	public void removeFromSynthesizer(Synthesizer synthesizer) {
		synthesizer.remove(this.egUnitGenerator);
	}

}
