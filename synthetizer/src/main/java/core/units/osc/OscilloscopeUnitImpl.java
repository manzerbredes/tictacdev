package core.units.osc;

import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;

import core.synthesizer.Synthesizer;
import core.units.AbstractUnit;
import javafx.scene.canvas.Canvas;

public class OscilloscopeUnitImpl extends AbstractUnit implements OscilloscopeUnit {
	
	private SynthetizerUnitOscillator unitOscillator;
	
	private Canvas canvas;
	
	public OscilloscopeUnitImpl() {
		this.unitOscillator = new SynthetizerUnitOscillator();
		this.setPortsNumbers(1,1);
	}

	public SynthetizerUnitOscillator getUnitOscillator() {
		return unitOscillator;
	}

	public void setUnitOscillator(SynthetizerUnitOscillator unitOscillator) {
		this.unitOscillator = unitOscillator;
	}

	public void start() {
		this.unitOscillator.start();
		// -- launch the GUI
		//OscilloscopeRectangleCanvas.dessinerSignal(this.unitOscillator.getSignalCapture(), canvas, oscillo_x, oscillo_y);
		// ---------
		//OscillateurViewer.setOscillateurModel(oscillateurModel);
		//OscillateurViewer.launchMethod(oscillateurModel);
	}

	public void stop() {
		// TODO stop the oscillo
		this.unitOscillator.stop();
	}

	public UnitInputPort getInputPort(int id) {
		if(id==0){
            return this.unitOscillator.input;
        }
        return null;
	}

	public UnitOutputPort getOutputPort(int id) {
		if(id==0){
            return this.unitOscillator.output;
        }
        return null;
	}

	public void addToSynthesizer(Synthesizer synthesizer) {
		synthesizer.add(this.unitOscillator);
	}

	public void removeFromSynthesizer(Synthesizer synthesizer) {
		synthesizer.remove(this.unitOscillator);
	}

}
