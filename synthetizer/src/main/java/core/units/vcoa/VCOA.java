package core.units.vcoa;

import core.units.Unit;

/**
 * Created by loic on 31/01/17.
 * TODO : Ajout de l'entrer fm (cf user storie)
 */
public interface VCOA extends Unit {

    /**
     * Define signal types
     */
    enum SIGNAL {SQUARE, TRIANGLE, SAWTOOTH};


    /**
     * Define Base frequency rules
     */
    int BASE_FREQUENCY_MIN=0;
    int BASE_FREQUENCY_MAX=5;

    /**
     * Define frequency rules
     */
    int FREQUENCY_MIN=0;
    int FREQUENCY_MAX=100;

    /**
     * Set the base frequency
     */
    void setBaseFrequency(int baseFrequency);

    /**
     * Get the base frequency
     * @return
     */
    int getBaseFrequency();

    /**
     * Set the frequency
     */
    void setFrequency(double frequency);

    /**
     * Get the frequency
     * @return
     */
    double getFrequency();

    /**
     * Set the signal type
     * @param signalType
     */
    void setSignalType(SIGNAL signalType);

    /**
     * Get the signal type
     * @return
     */
    SIGNAL getSignalType();
}
