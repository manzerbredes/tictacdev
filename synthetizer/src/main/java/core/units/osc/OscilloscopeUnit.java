package core.units.osc;

import com.jsyn.unitgen.UnitOscillator;

import core.units.Unit;

public interface OscilloscopeUnit extends Unit {

	SynthetizerUnitOscillator getUnitOscillator();

	void setUnitOscillator(SynthetizerUnitOscillator unitOscillator);

}