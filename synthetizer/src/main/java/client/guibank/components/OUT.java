
package client.guibank.components;

import client.guibank.components.element.Button;
import client.guibank.components.element.CircleSlider;
import client.guibank.components.element.Port;
import core.portObserver.PortObservable;
import core.portObserver.PortObserver;
import core.synthesizer.Synthesizer;
import core.units.out.Out;
import core.units.out.OutImpl;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sarah on 07/02/17.
 */
public class OUT extends Geometry implements Component, PortObservable {

    private Port inPort;
    private Button muteButton, back, next, delete, img;
    private CircleSlider volume;
    private List<PortObserver> observers = new ArrayList<PortObserver>();
    private Out out = new OutImpl();
    private double volumeValue;
    private int volumeAngle = 0;


    public OUT() {
        Synthesizer.getSynthesizer().add(this.out);
        Synthesizer.getSynthesizer().startUnit(this.out);

        this.width = 150;
        this.height = 250;
        this.volumeValue = this.out.getVolume();
    }

    private void buildElements() {

        this.inPort = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) - 120, out.getInputPort(0), true, this);

        this.muteButton = new Button("/imgs/mute.png", this.x + (this.width) - 25, this.y + (this.height / 2) - 120);
        this.img = new Button("/imgs/bloggif.png", this.x + (this.width - 15) - 100, this.y + (this.height / 2) - 65);

        this.back = new Button("/imgs/previous.png", this.x + this.width / 2 - 30, ((this.y + this.height / 2) + 90));
        this.next = new Button("/imgs/next.png", this.x + (this.width / 2), ((this.y + this.height / 2) + 90));

        this.volume = new CircleSlider("/imgs/button.png", 0, 0, this.volumeAngle);
        this.volume.move(this.x + this.width / 2 - this.volume.getWidth() / 2, (this.y + this.height / 2) + 40);
    }

    public void draw(GraphicsContext gc) {
        this.buildElements();
        gc.setFill(Color.valueOf("#626567"));
        gc.fillRect(this.x, this.y, this.width, this.height);
        gc.setStroke(Color.BLACK);
        gc.strokeRoundRect(this.x, this.y, this.width, this.height, 5, 5);

        gc.setFill(Color.WHITE);
        gc.setStroke(Color.WHITE);
        //gc.fillText("OUT", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));
        // gc.strokeText("OUT", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));


        //gc.fillText("IN", this.x + 16, this.y);
        this.inPort.draw(gc);
        this.muteButton.draw(gc);
        this.img.draw(gc);
        this.back.draw(gc);
        this.next.draw(gc);
        this.volume.draw(gc);
    }

    public void handleClick(double x, double y) {
        if (this.inPort.isClickedRect(x, y)) {
            connect(inPort);
        } else if (this.muteButton.isClickedRect(x, y)) {
            out.setMute(!out.isMute());
        } else if (back.isClickedRect(x, y)) {
            this.volumeValue -= 10;
            this.out.setVolume(this.volumeValue);
            this.updateAngle();
        } else if (next.isClickedRect(x, y)) {
            this.volumeValue += 10;
            this.out.setVolume(this.volumeValue);
            this.updateAngle();
        }


    }

    public void destroy() {
    }

    public boolean needToBeDestroy() {
        return false;
    }

    private void updateAngle() {
        this.volumeValue = this.out.getVolume();
        int angle = 0;
        if (this.volumeValue >= 50) {
            angle = (int) this.volumeValue - 50;
        } else {
            angle = (int) 50 - (int) this.volumeValue;
            angle = -angle;
        }
        angle = angle * 3;
        this.volumeAngle = angle;
    }

    public void connect(Port port) {
        for (PortObserver o : this.observers) {
            o.connect(port);
        }
    }

    public void disconnect(Port port) {
        for (PortObserver o : this.observers) {
            o.disconnect(port);
        }
    }

    public void addObserver(PortObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(PortObserver observer) {
        this.observers.remove(observer);

    }
}