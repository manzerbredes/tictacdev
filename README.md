# README #

Projet Synthétiseur de l'équipe Tic Tac Dev 

**REGARDEZ LA PARTIE BRANCHE PLUS BAS !!!!!**

### Etudiants ###

* Anis
* Aroua
* Loic
* Sarah
* Faiçal
* Chouaib

### Branches ###

*  `master`  ( dernière version fonctionnelle, **ne pas developper la dessus** )

*  `develop` ( version en cour de développement, **ne pas développer directement la dessus** )

*  `<nom-etudiant>` ( branche de l'étudiant **développer la dessus** )

### Règles ###

* Ne pas mettre en ligne les méta-données générées par votre IDE (ajouter au .gitignore).
