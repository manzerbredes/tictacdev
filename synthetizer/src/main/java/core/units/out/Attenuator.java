package core.units.out;

import com.jsyn.unitgen.UnitFilter;

/**
 * Created by chouaib on 03/02/17.
 */
public class Attenuator extends UnitFilter {
    private double attenuation = 0.5D;

    public void setAttenuation(double attenuation) {
        if(attenuation>=0 && attenuation<=1){
            this.attenuation = attenuation;
        }
    }

    public double getAttenuation() {
        return this.attenuation;
    }

    @Override
    public void generate(int start, int limit) {

        double[] inputs = this.input.getValues();
        double[] outputs = this.output.getValues();

        for (int i = start; i < limit; ++i) {
            double in = inputs[i];
            double out = this.attenuation * in;
            outputs[i] = out;
        }
    }
}
