package core.units.vcoa;

import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;
import com.jsyn.unitgen.*;
import config.Config;
import core.synthesizer.Synthesizer;
import core.units.AbstractUnit;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by loic on 31/01/17.
 */
public class VCOAImpl extends AbstractUnit implements VCOA {

    private SIGNAL signal=SIGNAL.SQUARE;
    private HashMap<SIGNAL, UnitOscillator> oscillators=new HashMap<SIGNAL, UnitOscillator>();
    private int baseFrequency=3; // La
    private double frequency=55; // 440
    private boolean started=false;
    private PassThrough outputReducer=new PassThrough();
    private FrequencyReducer inputFrequencyReducer =new FrequencyReducer();
    private PassThrough inputAmplitudeReducer=new PassThrough();

    /**
     * Constructor
     */
    public VCOAImpl() {
        // Configure oscillator
        this.oscillators.put(SIGNAL.SQUARE, new SquareOscillator());
        this.oscillators.put(SIGNAL.SAWTOOTH, new SawtoothOscillator());
        this.oscillators.put(SIGNAL.TRIANGLE, new TriangleOscillator());

        // Configure VCOA Ports numbers
        this.setPortsNumbers(1,1);

        // Connect output to the reducer
        for (Map.Entry<SIGNAL, UnitOscillator> entry : this.oscillators.entrySet()) {
            entry.getValue().output.connect(outputReducer.input);
        }

        // Connect input frequency to the reducer
        for (Map.Entry<SIGNAL, UnitOscillator> entry : this.oscillators.entrySet()) {
            inputFrequencyReducer.output.connect(entry.getValue().frequency);
        }

        // Connect input amplitude to the reducer
        for (Map.Entry<SIGNAL, UnitOscillator> entry : this.oscillators.entrySet()) {
            inputAmplitudeReducer.output.connect(entry.getValue().amplitude);
        }
        inputAmplitudeReducer.input.setMinimum(Config.AMPLITUDE_MIN);
        inputAmplitudeReducer.input.setMaximum(Config.AMPLITUDE_MAX);
        inputAmplitudeReducer.input.set(1.0);

        // Update Frequency
        this.inputFrequencyReducer.input.set(0);
        this.updateFrequency();

    }


    /**
     * @param baseFrequency
     * @see VCOA
     */
    public void setBaseFrequency(int baseFrequency) {
        if (baseFrequency >= this.BASE_FREQUENCY_MIN && baseFrequency <= this.BASE_FREQUENCY_MAX) {
            this.baseFrequency=baseFrequency;
            this.updateFrequency();
        }
    }


    /**
     * @return
     * @see VCOA
     */
    public int getBaseFrequency() {
        return this.baseFrequency;
    }


    /**
     * @param frequency
     * @see VCOA
     */
    public void setFrequency(double frequency) {
        if (frequency >= this.FREQUENCY_MIN && frequency <= this.FREQUENCY_MAX) {
            this.frequency=frequency;
            this.updateFrequency();
        }
    }


    /**
     * @return
     * @see VCOA
     */
    public double getFrequency() {
        return this.frequency;
    }


    /**
     * @param signalType
     * @see VCOA
     */
    public void setSignalType(SIGNAL signalType) {
       this.oscillators.get(signal).setEnabled(false);
       for(Map.Entry<SIGNAL, UnitOscillator> entry:this.oscillators.entrySet()){
           if(entry.getKey()!=signalType){
               entry.getValue().setEnabled(false);
           }
           else{
               if(this.started){
                   entry.getValue().setEnabled(true);
               }
               else{
                   entry.getValue().setEnabled(false);
               }
           }
       }
       this.signal=signalType;
    }


    /**
     * @return
     * @see VCOA
     */
    public SIGNAL getSignalType() {
        return this.signal;
    }


    /**
     * @see core.units.Unit
     */
    public void start() {
        this.oscillators.get(this.signal).setEnabled(true);
        this.started=true;
    }


    /**
     * @see core.units.Unit
     */
    public void stop() {
        this.oscillators.get(this.signal).setEnabled(false);
        this.started=false;
    }


    /**
     * @see core.units.Unit
     * @param id
     * @return
     */
    public UnitInputPort getInputPort(int id) {
        if(id==0){
            return this.inputFrequencyReducer.input;
        }
        else if(id==1){
            return this.inputAmplitudeReducer.input;
        }
        return null;
    }


    /**
     * @see core.units.Unit
     * @param id
     * @return
     */
    public UnitOutputPort getOutputPort(int id) {
        if(id==0){
            return outputReducer.output;
        }
        return null;
    }


    /**
     * @see core.units.Unit
     * @param synthesizer
     */
    public void addToSynthesizer(Synthesizer synthesizer) {
        synthesizer.add(inputFrequencyReducer);
        synthesizer.add(inputAmplitudeReducer);
        synthesizer.add(outputReducer);

        for (Map.Entry<SIGNAL, UnitOscillator> entry : this.oscillators.entrySet()) {
            synthesizer.add(entry.getValue());
            entry.getValue().setEnabled(false);
        }
    }


    /**
     * @see core.units.Unit
     * @param synthesizer
     */
    public void removeFromSynthesizer(Synthesizer synthesizer) {
        synthesizer.remove(inputFrequencyReducer);
        synthesizer.add(inputAmplitudeReducer);
        synthesizer.remove(outputReducer);
        for (Map.Entry<SIGNAL, UnitOscillator> entry : this.oscillators.entrySet()) {
            synthesizer.remove(entry.getValue());
        }
    }


    /**
     * Compute and connect the frequency
     */
    private void updateFrequency(){
        this.inputFrequencyReducer.setFrequency(this.frequency);
        this.inputFrequencyReducer.setBaseFrequency(this.baseFrequency);
    }

}
