package client.guibank.components;


import java.util.ArrayList;
import java.util.List;

import client.guibank.components.element.Button;
import client.guibank.components.element.Port;
import core.portObserver.PortObservable;
import core.portObserver.PortObserver;
import core.synthesizer.Synthesizer;
import core.units.osc.OscilloscopeRectangleCanvas;
import core.units.osc.OscilloscopeUnitImpl;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Created by aroua
 */
public class OSC extends Geometry implements Component, PortObservable {


    private Button delete;

    private static int OSCILLO_X = 150;

    private static int OSCILLO_Y = 280;

    private Port portIn;

    private Port portOut;

    OscilloscopeUnitImpl oscilloscopeUnitImpl;

    private GraphicsContext gc;

    private Thread refreshThreadForOsc;

    private boolean needToBeDestroy = false;

    private List<PortObserver> observers = new ArrayList<PortObserver>();


    public OSC() {
        this.oscilloscopeUnitImpl = new OscilloscopeUnitImpl();
        this.height = 250;
        this.width = 500;
    }

    private void buildElements() {
        this.delete = new Button("/imgs/delete.png", this.x + this.width - 25, this.y + 5);
        this.portIn = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) - 10, this.oscilloscopeUnitImpl.getInputPort(0), true, this);
        this.portOut = Port.getPort("/imgs/out.png", (this.x + this.width) - 40, this.y + (this.height / 2) - 10, this.oscilloscopeUnitImpl.getOutputPort(0), false, this);
    }

    public void draw(GraphicsContext gc) {
        this.buildElements();
        gc.setFill(Color.valueOf("#626567"));
        gc.fillRect(this.x, this.y, this.width, this.height);
        gc.setFill(Color.WHITE);
        gc.setStroke(Color.WHITE);
        gc.fillText("OSC", this.x + 140, this.y + 25, 200);
        gc.fillText("IN", this.x + 15, this.y + (this.height / 2) - 25, 100);
        gc.fillText("OUT", this.x + this.width - 50, this.y + (this.height / 2) - 25, 100);
        portIn.draw(gc);
        portOut.draw(gc);
        this.gc = gc;
        drawOscilloscope();
        delete.draw(gc);
        /*refreshThreadForOsc = new Thread(() -> {
            while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				drawOscilloscope();
			}
		} );
		refreshThreadForOsc.start();*/
    }

    private void drawOscilloscope() {
        OscilloscopeRectangleCanvas.initiateOscilloscope(gc, (int) this.x + 50, (int) this.y + 30);
        if (this.oscilloscopeUnitImpl != null && this.oscilloscopeUnitImpl.getUnitOscillator() != null &&
                this.oscilloscopeUnitImpl.getUnitOscillator().getSignalCapture() != null) {
            OscilloscopeRectangleCanvas.dessinerSignal(this.oscilloscopeUnitImpl.getUnitOscillator().getSignalCapture(), gc, (int) this.x + 50, (int) this.y + 30);
        }
    }

    public void handleClick(double x, double y) {
        if (portOut.isClicked(x, y)) {
            connect(portOut);
        } else if (this.portIn.isClickedRect(x, y)) {
            connect(portIn);
        } else if (this.delete.isClicked(x, y)) {
            this.needToBeDestroy = true;
        }
    }

    public void destroy() {
        disconnect(portOut);
        Synthesizer.getSynthesizer().stopUnit(this.oscilloscopeUnitImpl);
        Synthesizer.getSynthesizer().remove(this.oscilloscopeUnitImpl);
    }

    public boolean needToBeDestroy() {
        return this.needToBeDestroy;
    }

    public void connect(Port port) {
        for (PortObserver o : this.observers) {
            o.connect(port);
        }
    }

    public void disconnect(Port port) {
        for (PortObserver o : this.observers) {
            o.disconnect(port);
        }
    }

    public void addObserver(PortObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(PortObserver observer) {
        this.observers.remove(observer);

    }

}
