package client.guibank.components;

import client.guibank.components.element.Button;
import client.guibank.components.element.Port;
import core.portObserver.PortObservable;
import core.portObserver.PortObserver;
import core.synthesizer.Synthesizer;
import core.units.rep.Rep;
import core.units.rep.RepImpl;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chouaib on 11/02/17.
 */
public class REP extends Geometry implements Component, PortObservable {

    private Port inPort,outPort1,outPort2,outPort3;
    private Button delete;
    private Rep rep;
    private boolean needToBeDestroy=false;
    private List<PortObserver> observers = new ArrayList<PortObserver>();


    public REP() {
        this.width = 150;
        this.height = 150;
        this.rep = new RepImpl();
    }

    private void buildElements() {

        this.delete = new Button("/imgs/delete.png", this.x + this.width - 25, this.y+5);


        this.inPort = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) - 40, rep.getInputPort(0), true, this);

        this.outPort1 = Port.getPort("/imgs/out.png", this.x + 10, this.y + (this.height / 2) + 40, rep.getOutputPort(0), false, this);

        this.outPort2 = Port.getPort("/imgs/out.png", this.x + (this.width / 2) - 15, this.y + (this.height / 2) + 40, rep.getOutputPort(1), false, this);

        this.outPort3 = Port.getPort("/imgs/out.png", (this.x + this.width) - 40, this.y + (this.height / 2) + 40, rep.getOutputPort(2), false, this);

    }


    public void handleClick(double x, double y) {
        if (inPort.isClicked(x, y)) {
            connect(inPort);
        } else if (outPort1.isClicked(x, y)) {
            connect(outPort1);
        } else if (outPort2.isClicked(x, y)) {
            connect(outPort2);
        } else if (outPort3.isClicked(x, y)) {
            connect(outPort3);
        }
        else if(this.delete.isClicked(x,y)){
            this.needToBeDestroy=true;
        }
    }

    public void destroy() {
        disconnect(inPort);
        disconnect(outPort1);
        disconnect(outPort2);
        disconnect(outPort3);
        Synthesizer.getSynthesizer().stopUnit(this.rep);
        Synthesizer.getSynthesizer().remove(this.rep);
    }

    public boolean needToBeDestroy() {
        return this.needToBeDestroy;
    }

    public void draw(GraphicsContext gc) {
        this.buildElements();
        gc.setFill(Color.valueOf("#626567"));
        gc.fillRect(this.x, this.y, this.width, this.height);
        gc.setStroke(Color.BLACK);
        gc.strokeRoundRect(this.x, this.y, this.width, this.height, 5, 5);
        gc.setFill(Color.WHITE);
        gc.setStroke(Color.WHITE);
        gc.fillText("REP", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));
        gc.strokeText("REP", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));

        gc.fillText("IN", this.x + 18, this.y + (this.height / 2) - 42);
        gc.fillText("OUT 1", this.x + 8, this.y + (this.height / 2 + 35));
        gc.fillText("OUT 2", this.x + (this.width / 2) - 17, this.y + (this.height / 2 + 35));
        gc.fillText("OUT 3", this.x + this.width - 45, this.y + (this.height / 2 + 35));


        inPort.draw(gc);
        outPort1.draw(gc);
        outPort2.draw(gc);
        delete.draw(gc);
        outPort3.draw(gc);
    }

    public void connect(Port port) {
        for (PortObserver o : this.observers) {
            o.connect(port);
        }
    }

    public void disconnect(Port port) {
        for (PortObserver o : this.observers) {
            o.disconnect(port);
        }
    }

    public void addObserver(PortObserver observer) {
        this.observers.add(observer);

    }

    public void removeObserver(PortObserver observer) {
        this.observers.remove(observer);
    }
}
