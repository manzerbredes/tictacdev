package core.portObserver;

import client.guibank.components.element.Port;

/**
 * Created by loic on 10/02/17.
 */
public interface PortObserver {

    void connect(Port port);

    void disconnect(Port port);

}
