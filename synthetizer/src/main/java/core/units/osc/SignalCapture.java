package core.units.osc;

import java.util.ArrayList;
import java.util.List;

public class SignalCapture {
	
	private static final int LIMIT_ARRAY = 350;
	
	private List<Double> listBips = new ArrayList<Double>();
	
	public List<Double> getListBips() {
		return listBips;
	}

	private boolean needGuiUpdate = false;
	
	public SignalCapture() {}
	
	public boolean isNeedGuiUpdate() {
		return needGuiUpdate;
	}

	public void setNeedGuiUpdate(boolean needGuiUpdate) {
		this.needGuiUpdate = needGuiUpdate;
	}

	public void initCapture() {
		this.listBips = new ArrayList<Double>();
	}
	
	public void captureTable(double[] ds) {
		List<Double> toInclude = extractListDouble(ds);
		if (bipsNeedsToBeIncluded(toInclude)) {
			this.listBips.addAll(toInclude);
			if (this.listBips.size() > LIMIT_ARRAY) {
				this.listBips = this.listBips.subList(this.listBips.size() - LIMIT_ARRAY, this.listBips.size());
			}
			this.needGuiUpdate = true;
		}
	}

	private List<Double> extractListDouble(double[] pointsCapture) {
		List<Double> toInclude = new ArrayList<Double>();
		for (double i : pointsCapture) {
			toInclude.add(i);
		}
		return toInclude;
	}
	
	private boolean bipsNeedsToBeIncluded(List<Double> lbips) {
		//int index = Collections.indexOfSubList(this.listBips , lbips);
		return (this.listBips.size()<LIMIT_ARRAY);
	}
	
}
