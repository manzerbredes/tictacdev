package core.units.vcoa;

import com.jsyn.unitgen.UnitFilter;

/**
 * Created by loic on 08/02/17.
 */
public class FrequencyReducer extends UnitFilter {

    private int baseFrequency = 0;
    private double frequency = 0;

    @Override
    public void generate(int start, int limit) {
        double[] inputs = input.getValues();
        double[] outputs = output.getValues();

        for (int i = start; i < limit; i++) {
            outputs[i] = this.compute(inputs[i]);
        }

    }

    public int getBaseFrequency() {
        return baseFrequency;
    }

    public void setBaseFrequency(int baseFrequency) {
        this.baseFrequency = baseFrequency;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    private double compute(double vfm) {
        return this.frequency * Math.pow(2, this.baseFrequency + vfm);
    }
}