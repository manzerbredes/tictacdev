package config;

/**
 * Created by loic on 31/01/17.
 *
 * Define application configuration
 */
public class Config {
    /**
     * Defnie signal amplitude
     */
    public static final double FREQUENCY = 440.0;

    /**
     * Define AMPLITUDE
     */
    public static final double AMPLITUDE_MAX = 5.0;
    public static final double AMPLITUDE_MIN = -5.0;
}
