package client.guibank;

import javafx.scene.canvas.GraphicsContext;

/**
 * Created by loic on 07/02/17.
 */
public interface Drawable {

    void draw(GraphicsContext gc);
}
