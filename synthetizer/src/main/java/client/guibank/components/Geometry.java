package client.guibank.components;

/**
 * Created by loic on 07/02/17.
 */
public abstract class Geometry implements Component {

    protected double x = 0, y = 0;
    protected double width = 0,height = 0;


    protected boolean isClickedRect(double x, double y) {
        double xMax = this.x + this.width, yMax = this.y + this.height;

        if (x <= xMax && x >= this.x) {
            if (y <= yMax && y >= this.y) {
                return true;
            }
        }

        return false;
    }

    protected boolean isClickedCircle(double x, double y) {
        double rayon = this.width / 2;
        double x0 = this.x + rayon, y0 = this.y + rayon;

        return (Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0))) < rayon;
    }

    public boolean isClicked(double x, double y) {
        return this.isClickedRect(x, y);
    }

    public void move(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void resize(double width, double height){
        this.width=width;
        this.height=height;
    }

    public boolean isOver(Geometry g) {
        return this.x < (g.getX() + g.getWidth()) &&
                (this.x + this.width) > g.getX() &&
                this.y < (g.getY() + g.getHeight()) &&
                (this.y + this.height) > g.getY();
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }


}
