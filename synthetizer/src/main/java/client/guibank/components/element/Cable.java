package client.guibank.components.element;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.StrokeLineCap;

/**
 * Created by fadhloun on 03/02/17.
 */
public class Cable{


    public static Group draw(Port portIn, Port portOut, Color curentColor)
    {

        Group cableGroup=new Group();

        /**************************************************************************************************/

        double first_clickX= portIn.getX()+(portIn.getWidth()/2);
        double first_clickY= portIn.getY()+(portIn.getHeight()/2);
        double second_clickX=portOut.getX()+(portOut.getWidth()/2);
        double second_clickY=portOut.getY()+(portOut.getHeight()/2);
        double diffX = first_clickX- second_clickX;
        double diffY = Math.abs(first_clickY -second_clickY );
        CubicCurve curve4 = new CubicCurve();
        curve4.setStartX(first_clickX);
        curve4.setStartY(first_clickY);
        curve4.setControlX1(first_clickX- diffX / 3);
        curve4.setControlY1(first_clickY + 100 + diffY / 2);
        curve4.setControlX2(second_clickX + diffX / 3);
        curve4.setControlY2(second_clickY+ 100 + diffY / 2);
        curve4.setEndX(second_clickX);
        curve4.setEndY(second_clickY);
        curve4.setStroke(Color.BLACK);
        curve4.setStrokeWidth(8);
        curve4.setStrokeLineCap(StrokeLineCap.ROUND);
        curve4.setFill(null);
        cableGroup.getChildren().add(curve4);

        /**************************************************************************************************/

        CubicCurve curve5 = new CubicCurve();
        curve5.setStartX(first_clickX);
        curve5.setStartY(first_clickY);
        curve5.setControlX1(first_clickX- diffX / 3);
        curve5.setControlY1(first_clickY + 100 + diffY / 2);
        curve5.setControlX2(second_clickX + diffX / 3);
        curve5.setControlY2(second_clickY+ 100 + diffY / 2);
        curve5.setEndX(second_clickX);
        curve5.setEndY(second_clickY);
        curve5.setStroke(Color.color(curentColor.getRed(), curentColor.getGreen(), curentColor.getBlue(), 0.4));
        curve5.setStrokeWidth(7);
        curve5.setStrokeLineCap(StrokeLineCap.ROUND);
        curve5.setFill(null);
        cableGroup.getChildren().add(curve5);

        /**************************************************************************************************/

        CubicCurve curve6 = new CubicCurve();
        curve6.setStartX(first_clickX);
        curve6.setStartY(first_clickY);
        curve6.setControlX1(first_clickX- diffX / 3);
        curve6.setControlY1(first_clickY + 100 + diffY / 2);
        curve6.setControlX2(second_clickX + diffX / 3);
        curve6.setControlY2(second_clickY+ 100 + diffY / 2);
        curve6.setEndX(second_clickX);
        curve6.setEndY(second_clickY);
        curve6.setStroke(Color.color(curentColor.getRed(), curentColor.getGreen(), curentColor.getBlue(), 0.8));
        curve6.setStrokeWidth(6);
        curve6.setStrokeLineCap(StrokeLineCap.ROUND);
        curve6.setFill(null);
        cableGroup.getChildren().add(curve6);


        /**************************************************************************************************/

        CubicCurve curve7 = new CubicCurve();
        curve7.setStartX(first_clickX);
        curve7.setStartY(first_clickY);
        curve7.setControlX1(first_clickX- diffX / 3);
        curve7.setControlY1(first_clickY + 100 + diffY / 2);
        curve7.setControlX2(second_clickX + diffX / 3);
        curve7.setControlY2(second_clickY+ 100 + diffY / 2);
        curve7.setEndX(second_clickX);
        curve7.setEndY(second_clickY);
        curve7.setStroke(Color.color(curentColor.getRed(), curentColor.getGreen(), curentColor.getBlue(), 1.0));
        curve7.setStrokeWidth(3);
        curve7.setStrokeLineCap(StrokeLineCap.ROUND);
        curve7.setFill(null);
        cableGroup.getChildren().add(curve7);


        /***************************************************************************************/

        CubicCurve curve = new CubicCurve();
        curve.setStartX(first_clickX);
        curve.setStartY(first_clickY);
        curve.setControlX1(first_clickX- diffX / 3);
        curve.setControlY1(first_clickY + 100 + diffY / 2);
        curve.setControlX2(second_clickX + diffX / 3);
        curve.setControlY2(second_clickY+ 100 + diffY / 2);
        curve.setEndX(second_clickX);
        curve.setEndY(second_clickY);
        curve7.setStroke(Color.color(curentColor.getRed(), curentColor.getGreen(), curentColor.getBlue()));
        curve.setStrokeWidth(1);
        curve.setStrokeLineCap(StrokeLineCap.ROUND);
        curve.setFill(null);
        cableGroup.getChildren().add(curve);



/**************************************************************************************************/

        CubicCurve curve2 = new CubicCurve();
        curve2.setStartX(first_clickX);
        curve2.setStartY(first_clickY);
        curve2.setControlX1(first_clickX- diffX / 3);
        curve2.setControlY1(first_clickY + 100 + diffY / 2);
        curve2.setControlX2(second_clickX + diffX / 3);
        curve2.setControlY2(second_clickY+ 100 + diffY / 2);
        curve2.setEndX(second_clickX);
        curve2.setEndY(second_clickY);
        curve2.setStroke(new Color(1.0, 1.0, 1.0, 0.2));
        curve2.setStrokeWidth(4);
        curve2.setStrokeLineCap(StrokeLineCap.ROUND);
        curve2.setFill(null);
        cableGroup.getChildren().add(curve2);


        /**************************************************************************************************/

        CubicCurve curve3 = new CubicCurve();
        curve3.setStartX(first_clickX);
        curve3.setStartY(first_clickY);
        curve3.setControlX1(first_clickX- diffX / 3);
        curve3.setControlY1(first_clickY + 100 + diffY / 2);
        curve3.setControlX2(second_clickX + diffX / 3);
        curve3.setControlY2(second_clickY+ 100 + diffY / 2);
        curve3.setEndX(second_clickX);
        curve3.setEndY(second_clickY);
        curve3.setStroke(new Color(1.0, 1.0, 1.0, 0.2));
        curve3.setStrokeWidth(2);
        curve3.setStrokeLineCap(StrokeLineCap.ROUND);
        curve3.setFill(null);
        cableGroup.getChildren().add(curve3);
        return cableGroup;


    }


}
