package core.units.out;

import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;
import com.jsyn.unitgen.LineOut;
import core.synthesizer.Synthesizer;
import core.units.AbstractUnit;

/**
 * Created by chouaib on 02/02/17.
 */
public class OutImpl extends AbstractUnit implements Out {

    private final String MODULE_NAME = "OUT";  //a verifier
    private LineOut lineOut;
    private Attenuator attenuator = new Attenuator();
    private boolean mute = false;

    public OutImpl() {
        this.setPortsNumbers(1, 0);
        this.lineOut = new LineOut();
        attenuator.output.connect(0, lineOut.input, 0); /* Left side */
        attenuator.output.connect(0, lineOut.input, 1); /* Right side */
    }


    public boolean isMute() {
        return this.mute;
    }

    public void setMute(boolean mute) {
        this.mute = mute;
        if (this.isMute()) {
            this.stop();
        } else {
           this.start();
        }
    }

    public void start() {
        if (!this.isMute()) {
            this.lineOut.start();
        }
    }

    public void stop() {
        this.lineOut.stop();
    }


    public UnitInputPort getInputPort(int id) {
        if (id == 0) {
            return this.attenuator.input;
        }
        return null;
    }

    public UnitOutputPort getOutputPort(int id) {
        return null;
    }


    public void addToSynthesizer(Synthesizer synthesizer) {
        synthesizer.add(this.attenuator);
        synthesizer.add(this.lineOut);
    }

    public void removeFromSynthesizer(core.synthesizer.Synthesizer synthesizer) {
        synthesizer.remove(this.attenuator);
        synthesizer.remove(this.lineOut);
    }

    public void setVolume(double pourcent) {
        if(pourcent<=100 && pourcent>=0) {
            this.attenuator.setAttenuation(pourcent/100);
        }
    }

    public double getVolume() {
        return this.attenuator.getAttenuation()*100;
    }
}
