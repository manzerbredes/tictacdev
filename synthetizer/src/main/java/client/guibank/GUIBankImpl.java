package client.guibank;

import client.guibank.components.Component;
import client.guibank.components.Geometry;
import core.portObserver.PortObservable;
import core.synthesizer.Synthesizer;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.Iterator;

/**
 * Created by loic on 07/02/17.
 */
public class GUIBankImpl implements GUIBank {


    private ObservableList<Component> components = FXCollections.observableArrayList();
    private LinkPort linkPort;
    private GraphicsContext gc;
    private Canvas canvas;
    private DragNDrop dnd;


    public GUIBankImpl(Canvas canvas, Group cableGroup) {
        this.canvas = canvas;
        this.linkPort = new LinkPort(cableGroup);
        this.dnd = new DragNDrop(this, this.canvas);
        final GUIBank guiBank = this;
        this.canvas.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                guiBank.handeMouseClick(event.getX(), event.getY());
            }
        });
        this.components.addListener(new ListChangeListener<Component>() {
            public void onChanged(Change<? extends Component> c) {
                drawComponents();
            }
        });
        Synthesizer.getSynthesizer().start();
    }

    public void drawComponents() {
        this.canvas.getGraphicsContext2D().clearRect(0,0,this.canvas.getWidth(),this.canvas.getHeight());
        for (Component c : this.components) {
                ((Drawable) c).draw(this.canvas.getGraphicsContext2D());
        }
        this.linkPort.redraw();
    }

    public void addComponent(Component component){
        this.components.add(component);
        if(component instanceof PortObservable){
            ((PortObservable) component).addObserver(this.linkPort);
        }
    }


    public void handeMouseClick(double x, double y) {
        Iterator<Component> i =this.components.iterator();
        while(i.hasNext()){
            Component d=i.next();
            Geometry g = (Geometry) d;
            if (g.isClicked(x, y)) {
                d.handleClick(x, y);
                if(g.needToBeDestroy()){
                    g.destroy();
                    i.remove();
                }
                this.drawComponents();
            }
        }
    }


    public Component getClickedComponent(double x, double y) {
        for (Component d : this.components) {
            Geometry g = (Geometry) d;
            if (g.isClicked(x, y)) {
                return d;
            }
        }
        return null;
    }

    public void setCableColor(Color color) {
        this.linkPort.setColor(color);
    }


}
