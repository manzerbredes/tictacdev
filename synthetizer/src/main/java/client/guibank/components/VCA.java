package client.guibank.components;


import client.guibank.components.element.Button;
import client.guibank.components.element.CircleSlider;
import client.guibank.components.element.Port;
import core.portObserver.PortObservable;
import core.portObserver.PortObserver;
import core.synthesizer.Synthesizer;
import core.units.vcoa.VCOA;
import core.units.vcoa.VCOAImpl;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by loic on 07/02/17.
 */
public class VCA extends Geometry implements Component, PortObservable {


    private Button back,next, delete;
    private CircleSlider volumedB;
    private int volumedBAngle = 0;
    private Port portAm,portIn,portOut;
    private List<PortObserver> observers = new ArrayList<PortObserver>();
    private VCOA vcoa = new VCOAImpl();
    private boolean needToBeDestroy=false;


    public VCA() {
        Synthesizer.getSynthesizer().add(vcoa);
        Synthesizer.getSynthesizer().startUnit(vcoa);

        this.width = 250;
        this.height = 150;
    }

    private void buildElements() {
        this.delete = new Button("/imgs/delete.png", this.x + this.width - 25, this.y+5);

        this.back = new Button("/imgs/previous.png", this.x + this.width / 2 - 30, ((this.y + this.height / 2) + 25));
        this.next = new Button("/imgs/next.png", this.x + (this.width / 2), ((this.y + this.height / 2) + 25));

        this.volumedB = new CircleSlider("/imgs/button.png", 0, 0, this.volumedBAngle);
        this.volumedB.move(this.x + this.width / 2 - this.volumedB.getWidth() / 2, (this.y + this.height / 2) - this.volumedB.getHeight() / 2);

        this.volumedB = new CircleSlider("/imgs/button.png", 0, 0, this.volumedBAngle);
        this.volumedB.move(this.x + this.width / 2 - this.volumedB.getWidth() / 2, (this.y + this.height / 2) - this.volumedB.getHeight() / 2);


        this.portIn = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) - 40, vcoa.getInputPort(0), true, this);

        this.portAm = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) + 25, vcoa.getInputPort(1), true, this);

        this.portOut = Port.getPort("/imgs/out.png", (this.x + this.width) - 40, this.y + (this.height / 2) - 20, vcoa.getOutputPort(0), false, this);

    }

    public void draw(GraphicsContext gc) {

        this.buildElements();
        gc.setFill(Color.valueOf("#626567"));
        gc.fillRect(this.x, this.y, this.width, this.height);
        gc.setStroke(Color.BLACK);
        gc.strokeRoundRect(this.x, this.y, this.width, this.height, 5, 5);
        gc.setFill(Color.WHITE);
        gc.setStroke(Color.WHITE);
        gc.fillText("VCA", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));
        gc.strokeText("VCA", (this.width / 2 + this.x) - 14, this.y + (this.height / 6));

        gc.fillText("IN", this.x + 18, this.y + (this.height / 2) - 42);
        gc.fillText("AM", this.x + 16, this.y + (this.height / 2 + 23));
        gc.fillText("OUT", (this.x + this.width) - 40, this.y + (this.height / 2) - 22);


        back.draw(gc);
        next.draw(gc);
        volumedB.draw(gc);
        portIn.draw(gc);
        portAm.draw(gc);
        portOut.draw(gc);
        delete.draw(gc);
    }

    public void handleClick(double x, double y) {
        if (back.isClickedRect(x, y)) {
            volumedBAngle--;
        } else if (next.isClickedRect(x, y)) {
            volumedBAngle++;
        } else if (this.portIn.isClicked(x, y)) {
            this.connect(this.portIn);
        } else if (this.portOut.isClicked(x, y)) {
            this.connect(this.portOut);
        } else if (this.portAm.isClicked(x, y)) {
            this.connect(this.portAm);
        }
        else if(this.delete.isClicked(x,y)){
            this.needToBeDestroy=true;
        }

    }

    public void destroy() {
        disconnect(portAm);
        disconnect(portIn);
        disconnect(portOut);
        Synthesizer.getSynthesizer().stopUnit(this.vcoa);
        Synthesizer.getSynthesizer().remove(this.vcoa);
    }

    public boolean needToBeDestroy() {
        return this.needToBeDestroy;
    }


    public void connect(Port port) {
        for (PortObserver o : this.observers) {
            o.connect(port);
        }
    }

    public void disconnect(Port port) {
        for (PortObserver o : this.observers) {
            o.disconnect(port);
        }
    }

    public void addObserver(PortObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(PortObserver observer) {
        this.observers.remove(observer);

    }
}
