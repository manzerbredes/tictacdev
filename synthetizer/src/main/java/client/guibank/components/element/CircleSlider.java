package client.guibank.components.element;

import client.guibank.components.Geometry;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Created by sarah on 07/02/17.
 */
public class CircleSlider extends Geometry {

    private Image image;
    private int angle=0;

    public CircleSlider(String imgResource, double x, double y, int angle){
        image=new Image(this.getClass().getResource(imgResource).toString());
        this.x=x;
        this.y=y;
        this.width=image.getWidth();
        this.height=image.getHeight();
        this.angle=angle;
    }


    public void draw(GraphicsContext gc) {
        gc.save();
        gc.translate(this.x+(this.image.getWidth()/2),this.y+(this.image.getHeight()/2));
        gc.rotate(this.angle);
        gc.drawImage(this.image,-(this.image.getWidth()/2),-(this.image.getHeight()/2), this.width,this.height);
        gc.restore();
    }



    @Override
    public boolean isClicked(double x, double y){
        return this.isClickedCircle(x,y);
    }


    public void handleClick(double x, double y) {

    }

    public void destroy() {

    }

    public boolean needToBeDestroy() {
        return false;
    }
}