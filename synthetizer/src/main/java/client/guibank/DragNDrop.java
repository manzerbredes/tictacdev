package client.guibank;

import client.guibank.components.Component;
import client.guibank.components.Geometry;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;

/**
 * Created by loic on 08/02/17.
 */
public class DragNDrop {

    private GUIBank guiBank;
    private Component draggedComponent;
    private double factorX=0,factorY=0;
    private double lastX=0,lastY=0;
    private Canvas canvas;


    public DragNDrop(GUIBank guiBank, Canvas canvas){
        this.guiBank=guiBank;
        this.canvas=canvas;
        this.configure();
    }

    private void dragStart(double x, double y){
        this.draggedComponent=this.guiBank.getClickedComponent(x,y);
        if(this.draggedComponent!=null) {
            Geometry g = (Geometry) draggedComponent;
            this.factorX = x - g.getX();
            this.factorY = y - g.getY();
            this.lastX=g.getX();
            this.lastY=g.getY();
        }
    }

    private void dragStop(){
        this.draggedComponent=null;
    }

    private void configure(){
        final DragNDrop dragNDrop=this;

        canvas.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                dragStart(event.getX(),event.getY());
                event.consume();
            }
        });

        canvas.setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if(draggedComponent!=null){
                    Geometry o=(Geometry)draggedComponent;
                    o.move(event.getX()-factorX, event.getY()-factorY);
                    repairCoordinates(draggedComponent);
                    guiBank.drawComponents();
                }
                event.consume();
            }
        });

        canvas.setOnMouseReleased(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if(draggedComponent!=null) {
                    event.consume();
                    dragStop();
                }
            }
        });

    }


    private void repairCoordinates(Component c){
        Geometry g=(Geometry)c;
        double x=g.getX(),y=g.getY();
        double xMax=this.canvas.getWidth()-g.getWidth();
        double yMax=this.canvas.getHeight()-g.getHeight();

        if(x<0){
            g.move(0,g.getY());
        }
        else if(x > xMax){
            g.move(xMax, g.getY());
        }
        if(y<0){
            g.move(g.getX(), 0);
        }
        else if(y>yMax){
            g.move(g.getX(),yMax);
        }
    }


}
