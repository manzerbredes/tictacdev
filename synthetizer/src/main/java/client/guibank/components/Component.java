package client.guibank.components;

import client.guibank.Drawable;

/**
 * Created by loic on 07/02/17.
 */
public interface Component extends Drawable {

    void handleClick(double x, double y);

    void destroy();

    boolean needToBeDestroy();
}
