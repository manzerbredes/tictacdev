package core.units;

import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;
import java.util.List;

/**
 * Created by loic on 31/01/17.
 */
public interface Unit {

    /**
     * Start the unit
     */
    void start();

    /**
     * Stop the unit
     */
    void stop();

    /**
     * Get input port ids
     * @return
     */
    List<Integer> getInputPorts();

    /**
     * Get output port ids
     * @return
     */
    List<Integer> getOutputPorts();

    /**
     * Get a specific input port
     * @param id
     * @return
     */
    UnitInputPort getInputPort(int id);

    /**
     * Get a specific output port
     * @param id
     * @return
     */
    UnitOutputPort getOutputPort(int id);

    /**
     * Connect to the input port
     * @param inputId
     * @param unit
     * @param outputId
     */
    void connectInput(int inputId, Unit unit, int outputId);

    /**
     * Connect to the output port
     * @param outputId
     * @param unit
     * @param inputId
     */
    void connectOutput(int outputId, Unit unit, int inputId);

    /**
     * Disconnect specific unit from an input port
     * @param inputId
     * @param unit
     * @param outputId
     */
    void disconnectInput(int inputId, Unit unit, int outputId);

    /**
     * Disconnect specific unit from an output port
     * @param outputId
     * @param unit
     * @param inputId
     */
    void disconnectOutput(int outputId, Unit unit, int inputId);

    /**
     * Disconnect all unit from an input port
     * @param inputId
     */
    void disconnectAllInput(int inputId);

    /**
     * Disconnect all unit from an output port
     * @param ouputId
     */
    void disconnectAllOutput(int ouputId);

    /**
     * Add the unit to the synthesizer
     * @param synthesizer
     */
    void addToSynthesizer(core.synthesizer.Synthesizer synthesizer);

    /**
     * Remove the unit from the synthesizer
     * @param synthesizer
     */
    void removeFromSynthesizer(core.synthesizer.Synthesizer synthesizer);

}
