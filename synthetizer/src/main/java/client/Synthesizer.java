package client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Created by loic on 30/01/17.
 */
public class Synthesizer extends Application {

    /**
     * Application entry point
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/views/MainView.fxml"));
        primaryStage.setTitle("Synthlab Project");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

}