package client.guibank;

import client.guibank.components.Component;
import javafx.scene.paint.Color;

/**
 * Created by loic on 07/02/17.
 */
public interface GUIBank {

    void addComponent(Component component);

    void drawComponents();

    void handeMouseClick(double x, double y);

    Component getClickedComponent(double x, double y);

    void setCableColor(Color color);
}
