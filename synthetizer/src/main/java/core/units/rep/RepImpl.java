package core.units.rep;

import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;
import core.synthesizer.Synthesizer;
import core.units.AbstractUnit;

/**
 * Created by chouaib on 10/02/17.
 */
public class RepImpl extends AbstractUnit implements Rep {

    private com.jsyn.unitgen.PassThrough passThroughIn;
    private UnitInputPort input;
    private UnitOutputPort output1;
    private UnitOutputPort output2;
    private UnitOutputPort output3;

    public RepImpl() {
        this.setPortsNumbers(1, 3);
        passThroughIn = new com.jsyn.unitgen.PassThrough();

        com.jsyn.unitgen.PassThrough passThroughOut1 = new com.jsyn.unitgen.PassThrough();
        com.jsyn.unitgen.PassThrough passThroughOut2 = new com.jsyn.unitgen.PassThrough();
        com.jsyn.unitgen.PassThrough passThroughOut3 = new com.jsyn.unitgen.PassThrough();

        input = passThroughIn.input;

        output1 = passThroughOut1.output;
        output2 = passThroughOut2.output;
        output3 = passThroughOut3.output;

        // Internal mapping to make the replicator work.
        passThroughIn.output.connect(passThroughOut1.input);
        passThroughIn.output.connect(passThroughOut2.input);
        passThroughIn.output.connect(passThroughOut3.input);
    }

    /**
     * Activates the line out
     */
    public void activate() {
        passThroughIn.setEnabled(true);
    }

    /**
     * deactivates the line out
     */
    public void deactivate() {
        passThroughIn.setEnabled(false);
    }


    public boolean isActivated() {
        return passThroughIn.isEnabled();
    }

    public void start() {

    }

    public void stop() {

    }

    public UnitInputPort getInputPort(int id) {
        if (id == 0)
            return this.input;

        return null;
    }

    public UnitOutputPort getOutputPort(int id) {
        switch (id){
            case 0:
                return output1;
            case 1:
                return output2;
            case 2:
                return output3;
        }
        return  null;

    }


    public void addToSynthesizer(Synthesizer synthesizer) {
        //synthesizer.add(this.inputReducer);
        //synthesizer.add(this.lineOut);
    }

    public void removeFromSynthesizer(Synthesizer synthesizer) {

    }
}
