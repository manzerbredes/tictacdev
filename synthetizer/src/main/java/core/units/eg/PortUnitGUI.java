package core.units.eg;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class PortUnitGUI {
	
	public final static int LARGEUR = 250;
	
	private int x;
	
	private int y;
	
	private double min;
	
	private double max;
	
	private String name;
	
	private double value;
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	public PortUnitGUI(String name, int x, int y, double value, double min, double max) {
		this.name = name;
		this.value = value;
		this.x = x;
		this.y = y;
		this.min = min;
		this.max = max;
	}

	public PortUnitGUI(String name, int x, int y) {
		this.name = name;
		this.value = 0.0;
		this.x = x;
		this.y = y;
	}
	
	public void drawPortUnit(GraphicsContext gc) {
		gc.setFill(Color.BISQUE);
		gc.fillText(this.name, x, y + 10);
		gc.setFill(Color.BEIGE);
		gc.fillRect(x + 50, y, LARGEUR, 10);
		gc.setFill(Color.BISQUE);
		gc.fillText(String.valueOf(round(this.value, 5)), x + LARGEUR + 60, y + 10);
		gc.setFill(Color.YELLOWGREEN);
		gc.fillRect(x + 50, y, this.value*LARGEUR/(max-min), 10);
	}
	
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

	public boolean isClicked(double x2, double y2) {
		return (x2 -(x + 50))<LARGEUR && (x2 -(x + 50))>0 && (y2 - y)<10 && (y2 - y)>0;
	}

}
