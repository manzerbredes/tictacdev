package core.units.eg;

public class EGEnveloppeImpl extends com.jsyn.unitgen.EnvelopeDAHDSR {
	
	public EGEnveloppeImpl() {
		super();
		this.amplitude.set(5.0);
		attack.setup(0.0001, 0.001, 0.05);
		decay.setup(0.001, 0.001, 0.1);
		//sustain.setup(0.001, 0.01, 0.1);
		release.setup(0.001, 0.05, 1.0);
		hold.set(0.0);
		delay.set(0.0);
	}

}
