package core.units.rep;

import core.units.Unit;

/**
 * Created by chouaib on 10/02/17.
 */
public interface Rep extends Unit {

    /**
     * Activates the line out
     */
    public void activate();

    /**
     * deactivates the line out
     */

    public void deactivate();


    public boolean isActivated();
}
