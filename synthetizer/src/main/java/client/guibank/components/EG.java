package client.guibank.components;

import java.util.ArrayList;
import java.util.List;

import client.guibank.components.element.Port;
import core.portObserver.PortObservable;
import core.portObserver.PortObserver;
import core.synthesizer.Synthesizer;
import core.units.eg.EGUnitImpl;
import core.units.eg.PortUnitGUI;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Created by aroua
 */
public class EG extends Geometry implements Component, PortObservable {

	    private Port portIn;
	    private Port portOut;
	    
	    private PortUnitGUI attackBT;
	    
	    private PortUnitGUI decayBT;
	    
	    private PortUnitGUI sustainBT;
	    
	    private PortUnitGUI releaseBT;
	    
	    private EGUnitImpl egUnitImpl;

	    private GraphicsContext gc;
	    
	    private List<PortObserver> observers = new ArrayList<PortObserver>();
	    
	    private boolean needToBeDestroy=false;
	    
    public EG(){
    	this.egUnitImpl = new EGUnitImpl();
    	Synthesizer.getSynthesizer().add(egUnitImpl);
        this.height = 140;
        this.width = 500;
    }


    public void draw(GraphicsContext gc) {
    	this.portIn = Port.getPort("/imgs/in.png", this.x + 10, this.y + (this.height / 2) -10, this.egUnitImpl.getInputPort(0), true, this);

        this.portOut = Port.getPort("/imgs/out.png", (this.x + this.width) - 40, this.y + (this.height / 2) - 10, this.egUnitImpl.getOutputPort(0), false, this);
        
    	attackBT = new PortUnitGUI("attack", (int)this.x + 50, (int)this.y + 50, this.egUnitImpl.getEgUnitGenerator().attack.get(), this.egUnitImpl.getEgUnitGenerator().attack.getMinimum(), 
        		this.egUnitImpl.getEgUnitGenerator().attack.getMaximum());
        decayBT = new PortUnitGUI("decay", (int)this.x + 50, (int)this.y + 70, this.egUnitImpl.getEgUnitGenerator().decay.get(), this.egUnitImpl.getEgUnitGenerator().decay.getMinimum(), 
        		this.egUnitImpl.getEgUnitGenerator().decay.getMaximum());
        sustainBT = new PortUnitGUI("sustain", (int)this.x + 50, (int)this.y + 90, this.egUnitImpl.getEgUnitGenerator().sustain.get(), this.egUnitImpl.getEgUnitGenerator().sustain.getMinimum(), 
        		this.egUnitImpl.getEgUnitGenerator().sustain.getMaximum());
        releaseBT = new PortUnitGUI("release", (int)this.x + 50, (int)this.y + 110, this.egUnitImpl.getEgUnitGenerator().release.get(), this.egUnitImpl.getEgUnitGenerator().release.getMinimum(), 
        		this.egUnitImpl.getEgUnitGenerator().release.getMaximum());
        
        gc.setFill(Color.valueOf("#626567"));
		gc.fillRect(this.x,this.y,500,140);
        gc.strokeRoundRect(this.x,this.y,500,140,5,5);
        gc.setFill(Color.WHITE);
        gc.fillText("EG (Enveloppe Generator)",this.x + 140, this.y + 25, 200);
        gc.fillText("GATE",this.x + 15, this.y + 50, 100);
        gc.fillText("OUT",this.x + this.width - 50 ,this.y + 50,100);
        this.attackBT.drawPortUnit(gc);
        this.decayBT.drawPortUnit(gc);
        this.sustainBT.drawPortUnit(gc);
        this.releaseBT.drawPortUnit(gc);
        this.gc = gc;
        portIn.draw(gc);
        portOut.draw(gc);

    }

    public void handleClick(double x, double y){
        if (isClickedOnADSR(x, y)) {
        	updatePortUnits(x, y, gc);
        }
        if (portOut.isClicked(x, y)){
            connect(portOut);
        }
		else if (this.portIn.isClickedRect(x, y)) {
            connect(portIn);
        }
    }
    
    public boolean isClickedOnADSR(double x, double y) {
		return this.attackBT.isClicked(x,y) || this.decayBT.isClicked(x,y) || this.sustainBT.isClicked(x,y) || this.releaseBT.isClicked(x,y);
	}
	
	public void updatePortUnits(double x_click, double y_click, GraphicsContext gc) {
		if (this.attackBT.isClicked(x_click,y_click)) {
			double clicked_value = (this.attackBT.getMax() - this.attackBT.getMin())*(x_click - this.attackBT.getX() - 50)/PortUnitGUI.LARGEUR;
			this.attackBT.setValue(clicked_value);
			this.egUnitImpl.getEgUnitGenerator().attack.set(clicked_value);
		}
		else if (this.decayBT.isClicked(x_click,y_click)) {
			double clicked_value = (this.decayBT.getMax() - this.decayBT.getMin())*(x_click - this.decayBT.getX() - 50)/PortUnitGUI.LARGEUR;
			this.decayBT.setValue(clicked_value);
			this.egUnitImpl.getEgUnitGenerator().decay.set(clicked_value);
		}
		else if (this.sustainBT.isClicked(x_click,y_click)) {
			double clicked_value = (this.sustainBT.getMax() - this.sustainBT.getMin())*(x_click - this.sustainBT.getX() - 50)/PortUnitGUI.LARGEUR;
			this.sustainBT.setValue(clicked_value);
			this.egUnitImpl.getEgUnitGenerator().sustain.set(clicked_value);
		}
		else if (this.releaseBT.isClicked(x_click,y_click)) {
			double clicked_value = (this.releaseBT.getMax() - this.releaseBT.getMin())*(x_click - this.releaseBT.getX() - 50)/PortUnitGUI.LARGEUR;
			this.releaseBT.setValue(clicked_value);
			this.egUnitImpl.getEgUnitGenerator().release.set(clicked_value);
		}
		draw(gc);
	}

	public void destroy() {
        disconnect(portOut);
        Synthesizer.getSynthesizer().stopUnit(this.egUnitImpl);
        Synthesizer.getSynthesizer().remove(this.egUnitImpl);
    }

    public boolean needToBeDestroy() {
        return this.needToBeDestroy;
    }

    public void connect(Port port) {
        for (PortObserver o : this.observers) {
            o.connect(port);
        }
    }

    public void disconnect(Port port) {
        for (PortObserver o : this.observers) {
            o.disconnect(port);
        }
    }

    public void addObserver(PortObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(PortObserver observer) {
        this.observers.remove(observer);

    }



}
