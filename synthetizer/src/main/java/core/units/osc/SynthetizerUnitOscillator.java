package core.units.osc;

import java.util.Arrays;

import com.jsyn.unitgen.UnitFilter;

public class SynthetizerUnitOscillator extends UnitFilter {
	
	private SignalCapture signalCapture = new SignalCapture();
	
	@Override
	public void generate(int start, int limit) {
		double[] inputs = input.getValues();
        double[] outputs = output.getValues();

        for (int i = start; i < limit; i++) {
            outputs[i] =  inputs[i];
        }
        
        signalCapture.captureTable(Arrays.copyOfRange(inputs, start, limit));
	}

	public SignalCapture getSignalCapture() {
		return signalCapture;
	}

	public void setSignalCapture(SignalCapture signalCapture) {
		this.signalCapture = signalCapture;
	}

}