package client.guibank.components.element;

import client.guibank.components.Geometry;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Created by loic on 07/02/17.
 */
public class Button extends Geometry {


    private Image image;

    public Button(String imgResource, double x, double y){
        image=new Image(this.getClass().getResource(imgResource).toString());
        this.x=x;
        this.y=y;
        this.width=image.getWidth();
        this.height=image.getHeight();
    }


    public void draw(GraphicsContext gc) {
        gc.drawImage(this.image,this.x,this.y);
    }


    public void handleClick(double x, double y) {

    }

    public void destroy() {

    }

    public boolean needToBeDestroy() {
        return false;
    }
}
