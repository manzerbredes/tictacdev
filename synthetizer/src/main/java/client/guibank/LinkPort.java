package client.guibank;

import client.guibank.components.element.Cable;
import client.guibank.components.element.Port;
import com.jsyn.ports.UnitInputPort;
import com.jsyn.ports.UnitOutputPort;
import core.portObserver.PortObserver;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by loic on 10/02/17.
 */
public class LinkPort implements PortObserver {

    private List<CableElement> cables=new ArrayList<CableElement>();
    private Port lastClicked = null;
    private Group cableGroup;
    private Color color=Color.ALICEBLUE;

    public LinkPort(Group cableGroup) {
        this.cableGroup = cableGroup;
    }

    public void connect(Port port) {
        if (this.lastClicked == null)
            this.lastClicked = port;
        else {
            if (port.getComponent() != this.lastClicked.getComponent()) {
                if (port.isInput() != this.lastClicked.isInput()) {
                    UnitInputPort input = null;
                    UnitOutputPort output = null;
                    if (port.isInput()) {
                        input = (UnitInputPort) port.getUnitPort();
                        output = (UnitOutputPort) this.lastClicked.getUnitPort();
                    } else {
                        input = (UnitInputPort) this.lastClicked.getUnitPort();
                        output = (UnitOutputPort) port.getUnitPort();
                    }
                    boolean alreadyConnected=false;
                    for(CableElement e:this.cables){
                        if(e.equals(port,lastClicked)){
                            alreadyConnected=true;
                        }
                    }
                    if(!alreadyConnected){
                        input.connect(output);
                        CableElement element=new CableElement(cableGroup,port,lastClicked,input,output,this.color);
                        this.cables.add(element);
                        element.draw();
                        this.lastClicked=null;
                    }
                    else{
                        this.lastClicked=port;
                    }

                }
                else {
                    this.lastClicked=port;
                }
            }


        }

    }

    public void setColor(Color color) {
        this.color = color;
    }

    private final Random r=new Random();

    public void disconnect(Port port){
        for(CableElement e:this.cables){
            if(e.contain(port)){
                e.disconnect();
            }
        }
    }

    public void redraw() {
        Iterator<CableElement> i = this.cables.iterator();
        while (i.hasNext()) {
            CableElement c=i.next();
            if (c.isDrawable()) {
                c.draw();
            } else {
                i.remove();
            }
        }
    }

    private class CableElement{
        private Group root;
        private Group cable=new Group();
        private Port input;
        private Port output;
        private UnitInputPort unitInputPort;
        private UnitOutputPort unitOutputPort;
        private boolean drawable=true;
        private Color color;

        public CableElement(Group root, Port input, Port output, UnitInputPort unitInputPort, UnitOutputPort unitOutputPort, Color color) {
            this.root = root;
            this.input = input;
            this.output = output;
            this.unitInputPort = unitInputPort;
            this.unitOutputPort = unitOutputPort;
            this.color=color;
        }

        public void draw(){
            if(this.drawable) {
                this.cable.getChildren().clear();
                this.cable = Cable.draw(this.input,this.output, this.color);
                this.cable.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                    disconnect();
                    }
                });
                root.getChildren().add(cable);
            }
        }
        public void disconnect(){
            root.getChildren().remove(cable);
            unitInputPort.disconnect(unitOutputPort);
            drawable=false;
        }
        public boolean isDrawable(){
            return this.drawable;
        }

        public boolean equals(Port input, Port output){
            return (this.input==input || this.output==output || this.output==input || this.input==output);
        }

        public boolean contain(Port port){
            return (this.input==port || this.output==port);
        }
    }
}
