package core.units.out;

/**
 * Created by chouaib on 02/02/17.
 */

import core.units.Unit;

/**
 * module de sortie pour transférer le signal audio produit par un montage vers une sortie audio du PC
 **/
public interface Out extends Unit {

    /**
     * Check if the out is mute or not
     *
     * @return
     */
    boolean isMute();

    /**
     * Apply a mute state
     *
     * @param mute
     */
    void setMute(boolean mute);

    /**
     * Set the volume
     * @param pourcent
     */
    void setVolume(double pourcent);

    /**
     * Get the volume
     * @return
     */
    double getVolume();
}
